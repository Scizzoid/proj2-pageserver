from flask import Flask, render_template, request

app = Flask(__name__)
@app.errorhandler(404)
def file_not_found():
    return render_template('404.html')
    
@app.route("/")
def index():
    page = request.path
    if ("~" in page) or ("//" in page) or (".." in page):
        return render_template('403.html')
    
    else:
        return render_template(page)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
